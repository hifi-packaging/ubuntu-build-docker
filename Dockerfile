FROM ubuntu:latest
MAINTAINER Dale Glass <daleglass@gmail.com>

ARG linuxdeployqt_url=https://github.com/probonopd/linuxdeployqt/releases/download/6/linuxdeployqt-6-x86_64.AppImage
ARG appimagetool_url=https://github.com/AppImage/AppImageKit/releases/download/12/appimagetool-x86_64.AppImage


# This is a work in progress. The final intention is to build RPM packages of HiFi.
#
# Notes:
#
# Build fails on Fedora 30 due to statx calls failing with EPERM. 
# This is a docker issue: https://github.com/moby/moby/pull/36417
# Can be worked around by:
#     docker run --security-opt seccomp=statx_fixed.json
#
# To use strace inside the container, run with:
#     docker run --cap-add SYS_PTRACE



RUN apt update
RUN apt -y -u dist-upgrade


# Tools absolutely needed to build at all
RUN apt install -y build-essential qtbase5-dev libssl-dev python git cmake libnss3

#RUN dnf install -y gcc gcc-c++ cmake git python qt5-devel qt5-qtwebengine-devel openssl-devel

# Installed by vcpkg, we should transition to these eventually
#  * glslang[core]:x64-linux
#    hifi-host-tools[core]:x64-linux
#  * hifi-scribe[core]:x64-linux
#  * spirv-tools[core]:x64-linux
#
#
#  * bullet3[core]:x64-linux
#  * draco[core]:x64-linux
#  * etc2comp[core]:x64-linux
#  * glm[core]:x64-linux
#    hifi-client-deps[core]:x64-linux
#  * hifi-deps[core]:x64-linux
#  * nlohmann-json[core]:x64-linux
#  * nvtt[core]:x64-linux
#  * openexr[core]:x64-linux
#  * sdl2[core]:x64-linux
#  * tbb[core]:x64-linux
#  * vulkanmemoryallocator[core]:x64-linux
#  * zlib[core]:x64-linux

#missing: glslang-dev spirv-tools-dev spirv-headers-dev spirv-tools 
RUN apt install -y libopenexr-dev zlib1g-dev libsdl2-dev


# Uncertain
RUN apt install -y libvulkan-dev  libquazip-dev libquazip5-dev

# Additional tools not required for an actual build
# xz and pigz both can compress in parallel and are useful for making tarballs.
RUN apt install -y xz-utils pigz rsync libfuse2 wget patchelf openssh-client fuse

RUN wget "$linuxdeployqt_url" -O /usr/local/bin/linuxdeployqt
RUN wget "$appimagetool_url" -O /usr/local/bin/appimagetool

RUN chmod 755 /usr/local/bin/linuxdeployqt /usr/local/bin/appimagetool



# Missing
# * bullet3 - bullet, version 3? F30 only has 2.87
# * draco
# * etc2comp
# * SDL-mirror
# * nlohmann-json

# Missing, optional:
# * Leap Motion

# Optional debug tools
RUN apt install -y strace vim


# Build with:
# mkdir build
# cd build
# cmake .. -DOpenGL_GL_PREFERENCE:STRING=GLVND

# We want to build stuff as a normal user
RUN useradd -m -u 2000  hifi
USER hifi

ENTRYPOINT /bin/bash

